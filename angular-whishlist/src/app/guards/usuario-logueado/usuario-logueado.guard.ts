import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './../../services/auth.service';

@Injectable({
  providedIn: 'root'
})
//state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
export class UsuarioLogueadoGuard implements CanActivate {
  constructor(private authService: AuthService){}
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean {
    const isLoggedIn = this.authService.isLoggedIn();
    console.log('CantActivate', isLoggedIn);
    return true;
  }
}
