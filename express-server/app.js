var express = require('express'), cors = require('cors');
var app = express();

app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("server running on 3000") );

var ciudades = ["paris", "madrid", "barcelona", "barranquilla", "hong kong", "hanoi"];
app.get(
	"/ciudades", 
	(req, res, next) => res.json(
		ciudades.filter(
			(c) => c.toLowerCase().indexOf(
					req.query.q.toString().toLowerCase()
				) > -1
			)
		)
	);

var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
	console.log(req.body);
	//misDestinos = req.body;
	misDestinos.push(req.body.nuevo);
	console.log(misDestinos);
	res.json(misDestinos);
});
var traducciones= {};
traducciones['es'] = {};
traducciones['en'] = {};
traducciones['fr'] = {};

traducciones['es']['HOLA'] = 'Hola';
traducciones['es']['ruteo_simple'] = 'Ruteo simple';

traducciones['en']['HOLA'] = 'Hi';
traducciones['en']['ruteo_simple'] = 'Simple routing';

traducciones['fr']['HOLA'] = 'Bienvenue';
traducciones['fr']['ruteo_simple'] = 'Routage simple';
/*app.get("/api/translation", (req, res, next) => res.json([
	{
		lang: req.query.lang, 
		key:'HOLA',value:traducciones[req.query.lang]['HOLA'],
		lang: req.query.lang, 
		key:'ruteo_simple', value:traducciones[req.query.lang]['ruteo_simple']
	}
]));*/
app.get("/api/translation", function(req, res, next){
	res.json([
		{
			lang: req.query.lang, 
			key:'ruteo_simple', value:traducciones[req.query.lang]['ruteo_simple'],
			lang: req.query.lang, 
			key:'HOLA', value:traducciones[req.query.lang]['HOLA']
		}])
}
);
